import "./styles.css";
import React from "react";

interface Gift {
  name: string;
}
const defaultGifts: Gift[] = [];
const defaultNewGift: Gift = { name: "" };
export default function App() {
  const [gifts, setGifts] = React.useState(defaultGifts);
  const [newGift, setNewGift] = React.useState(defaultNewGift);

  const handlSaveGift = () => {
    setGifts([...gifts, newGift]);
    setNewGift({ name: "" });
    console.log(gifts);
  };

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNewGift({ name: e.target.value });
  };
  const handleDelete = (id: number) => {
    const tempGift = [...gifts];
    tempGift.splice(id, 1);
    setGifts(tempGift);
    console.log(gifts);
  };

  return (
    <div className="App">
      <div className="container">
        <form
          onSubmit={(e) => {
            e.preventDefault();
            handlSaveGift();
          }}
        >
          <h1>Regalos</h1>
          <input
            ref={(input) => input && input.focus()}
            placeholder="Agrega un regalo"
            required
            type="text"
            onChange={handleInputChange}
            name="name"
            value={newGift.name}
          ></input>
          <button className="Button-Agregar" type="submit">
            Agregar
          </button>
        </form>
        <ul>
          {gifts.map((Gift, id) => (
            <li key={id}>
              {Gift.name}{" "}
              <button
                className="Button-Error"
                key={id}
                onClick={() => handleDelete(id)}
              >
                Delete
              </button>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}
